import React, {useState} from 'react';
import {Container, Content, Form, Item, Input, Label} from 'native-base';
import {
  ImageBackground,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Axios from 'axios';

const resizeMode = 'center';
const ResetPassword = ({route, navigation}) => {
  const {mobile} = route.params;
  const [is_browser, setIs_browser] = useState(false);
  const [reset_password, setReset_password] = useState(false);

  const handleOtp = () => {
    setIs_browser(true);
    setReset_password(true);
    Axios.post('http://139.59.3.114/act/api/raiseotp/', {
      mobile: mobile,
      device_id: '20:00:00:00:00:00',
      is_browser: is_browser,
      reset_password: reset_password,
    }).then(res => {
      console.log('raise', res);
      if (res.status === 200) {
        navigation.navigate('NewPassword', {mobile: mobile});
      }
    });
    // navigation.navigate('NewPassword');
  };

  return (
    <Container style={styles.container}>
      <ImageBackground
        style={{
          backgroundColor: '#ccc',
          flex: 1,
          resizeMode,
          position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        source={require('../assets/login_background.png')}>
        <Content>
          <Image
            style={styles.img}
            source={require('../assets/field_high.png')}
          />
          <Form style={styles.formContainer}>
            <Item floatingLabel last style={{width: 350}}>
              <Label style={styles.labelColor}>User ID/Phone Number</Label>
              <Input style={styles.input} value={mobile} />
            </Item>
          </Form>
          <View style={styles.btnContainer}>
            <TouchableOpacity style={styles.userBtn} onPress={handleOtp}>
              <Text style={styles.btnTxt}>SEND OTP</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </ImageBackground>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  img: {
    marginLeft: '10%',
    marginTop: '20%',
    height: 60,
    width: 240,
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '10%',
  },
  labelColor: {
    color: 'white',
  },
  input: {
    color: 'white',
  },
  btnContainer: {
    marginTop: '10%',
  },
  userBtn: {
    borderRadius: 8,
    backgroundColor: '#025678',
    padding: 16,
    width: '100%',
  },
  btnTxt: {
    textAlign: 'center',
    color: 'white',
  },
});

export default ResetPassword;
