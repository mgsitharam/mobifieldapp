import React, {Component} from 'react';
import {View, Image, ImageBackground} from 'react-native';

class SplashScreen extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('Login');
      // this.props.navigation.navigate('ReceiptInCard');
    }, 5000);
  }

  render() {
    const resizeMode = 'center';
    return (
      //   <View style={{backgroundColor: '#ccc'}}>
      <ImageBackground
        style={{
          backgroundColor: '#ccc',
          display: 'flex',
          flex: 3,
          resizeMode,
          // position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        source={require('../assets/mobisplash.png')}
      />
      //   </View>
    );
  }
}

export default SplashScreen;
