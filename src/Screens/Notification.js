import React, {Component} from 'react';
import { View, StyleSheet, Text } from 'react-native';

class Notification extends Component{
    render(){
        return (
            <View style={styles.container}>
                <Text style={styles.txt}>You have no pending vouchers</Text>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    txt:{
        fontSize: 24
    }
})


export default Notification;