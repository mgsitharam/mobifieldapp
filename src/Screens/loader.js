import React, { Component } from 'react'
import {
  View,
} from 'react-native'

import{
  WaveIndicator,
  UIActivityIndicator,
  PacmanIndicator,
  BarIndicator,
  MaterialIndicator,
} from 'react-native-indicators'

export default class Loader extends Component {

  render() {
    return (
      <View>
        <MaterialIndicator animating={true} color='#0f49a6' style={{alignContent:'center'}} />
      </View>
    );
  }
}


