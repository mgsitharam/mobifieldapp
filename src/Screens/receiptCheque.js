import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Card, CardItem, Picker, Item, Label, Input, DatePicker, Container, Content, Header, Left, Body, Right, Title, Subtitle, Button } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler';
import { faPaperPlane, faArrowLeft, faPencilAlt, faCamera, faMicrophone, } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';


export default class ReceiptCheque extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDayBook: "key0",
            selectedCustType: "custType0",
            chosenDate: new Date(),
            validItems: false,
        };
        this.setDate = this.setDate.bind(this);
    }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });
    }

    onDayBookChange(value: string) {
        this.setState({
            selectedDayBook: value
        });
    }
    onCustTypeChange(value: string) {
        this.setState({
            selectedCustType: value
        });
    }

    render() {
        return (
            // <View style={{ backgroundColor: 'red' }}>
            <Container>
                {/* <HeaderTitle notification={faPaperPlane} {...props} /> */}
                {/* <Header style={{ backgroundColor: '#3399ff' }}>
                    <Left>
                        <FontAwesomeIcon
                            icon={faArrowLeft}
                            size="20"
                            style={{
                                color: 'white',
                            }}
                        />
                    </Left>
                    <Body>
                        <Title>Cheque Receipt</Title>
                        <Subtitle style={styles.subT}>AL Vijayawada - Mobibooks</Subtitle>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            onPress={() => props.navigation.navigate('Notification')}>
                            <FontAwesomeIcon
                                icon={faPaperPlane}
                                size="20"
                                style={{
                                    color: 'white',
                                }}
                            />
                        </Button>
                    </Right>
                </Header> */}
                <Content style={{ backgroundColor: 'gainsboro' }}>
                    <ScrollView>
                        <Card style={styles.cardContainer}>
                            <CardItem header style={styles.cardContainer} transparent>
                                <Text style={styles.cardHeaderText}>DayBook*</Text>

                            </CardItem>
                            <CardItem style={styles.cardContainer} transparent>
                                <Picker
                                    mode="dropdown"
                                    placeholder={this.state.selectedDayBook}
                                    placeholderStyle={{ color: "#2874F0" }}
                                    note={false}
                                    selectedValue={this.state.selectedDayBook}
                                    onValueChange={this.onDayBookChange.bind(this)}
                                >
                                    <Picker.Item label="Wallet" value="key0" />
                                    <Picker.Item label="ATM Card" value="key1" />
                                    <Picker.Item label="Debit Card" value="key2" />
                                    <Picker.Item label="Credit Card" value="key3" />
                                    <Picker.Item label="Net Banking" value="key4" />
                                </Picker>
                            </CardItem>
                        </Card>
                        <Card style={styles.cardContainer}>
                            <Body style={styles.cardContainer}>
                                <CardItem header style={styles.cardContainer} transparent>
                                    <Text style={styles.cardHeaderText}>Received From</Text>
                                </CardItem>
                                <CardItem style={styles.cardContainer} transparent>
                                    <Body style={{ flexDirection: "column", alignItems: 'center' }}>
                                        <Item floatingLabel>
                                            <Label style={styles.labelColor}>Party Name*</Label>
                                            <Input style={styles.input} />
                                        </Item>
                                        <Item floatingLabel last>
                                            <Label style={styles.labelColor}>Mobile*</Label>
                                            <Input keyboardType="number-pad" style={styles.input} />
                                        </Item>
                                    </Body>
                                </CardItem>
                                {this.state.validItems ? (

                                    <CardItem>
                                        <Body>
                                            <Item floatingLabel>
                                                <Label style={styles.labelColor}>Print Name*</Label>
                                                <Input style={styles.input} />
                                            </Item>
                                            <Item floatingLabel last>
                                                <Label style={styles.labelColor}>Reference Name</Label>
                                                <Input style={styles.input} />
                                            </Item>
                                            <Item floatingLabel>
                                                <Label style={styles.labelColor}>Email</Label>
                                                <Input style={styles.input} />
                                            </Item>
                                            <Item floatingLabel last>
                                                <Label style={styles.labelColor}>Billing Address Line 1</Label>
                                                <Input style={styles.input} />
                                            </Item>
                                            <Item floatingLabel>
                                                <Label style={styles.labelColor}>Billing Address Line 2</Label>
                                                <Input style={styles.input} />
                                            </Item>
                                            <Item floatingLabel last>
                                                <Label style={styles.labelColor}>City</Label>
                                                <Input style={styles.input} />
                                            </Item>
                                            <Item floatingLabel>
                                                <Label style={styles.labelColor}>Pin</Label>
                                                <Input keyboardType="number-pad" style={styles.input} />
                                            </Item>
                                            <Item floatingLabel last>
                                                <Label style={styles.labelColor}>STATE</Label>
                                                <Input style={styles.input} />
                                            </Item>
                                            <Item floatingLabel last>
                                                <Label style={styles.labelColor}>PAN</Label>
                                                <Input style={styles.input} />
                                            </Item>
                                            <Item floatingLabel>
                                                <Label style={styles.labelColor}>GSTIN</Label>
                                                <Input style={styles.input} />
                                            </Item>
                                        </Body>
                                    </CardItem>
                                ) : (
                                        <TouchableOpacity
                                            style={styles.more}
                                            onPress={() => this.setState({ validItems: !this.state.validItems })}>
                                            <Text style={styles.moreText}>MORE</Text>
                                        </TouchableOpacity>
                                    )}
                                <CardItem header style={styles.cardContainer} transparent>
                                    <Text style={styles.cardHeaderText}>Customer Type</Text>
                                </CardItem>
                                <CardItem>
                                    <Item>
                                        <Picker
                                            mode="dropdown"
                                            placeholder={this.state.selectedCustType}
                                            placeholderStyle={{ color: "#2874F0" }}
                                            note={false}
                                            selectedValue={this.state.selectedCustType}
                                            onValueChange={this.onCustTypeChange.bind(this)}
                                        >
                                            <Picker.Item label="Unregistered" value="custType0" />
                                            <Picker.Item label="Registered" value="custType1" />
                                            <Picker.Item label="Composite" value="custType02" />
                                            <Picker.Item label="Sez" value="custType03" />
                                        </Picker>
                                    </Item>
                                </CardItem>
                            </Body>
                        </Card>
                        <Card style={styles.cardContainer}>
                            <CardItem header transparent style={styles.cardContainer}>
                                <Body style={{ flexDirection: "column", alignItems: 'center' }}>
                                    <Item floatingLabel>
                                        <Label style={styles.labelColor}>Amount</Label>
                                        <Input keyboardType='number-pad' style={styles.input} />
                                    </Item>
                                    <Item floatingLabel last>
                                        <Label style={styles.labelColor}>Narration</Label>
                                        <Input style={styles.input} />
                                    </Item>
                                    <Item floatingLabel>
                                        <Label style={styles.labelColor}>Paid to*</Label>
                                        <Input style={styles.input} />
                                    </Item>
                                    <Item floatingLabel last>
                                        <Label style={styles.labelColor}>Reference Name</Label>
                                        <Input style={styles.input} />
                                    </Item>
                                    {/* <Item floatingLabel> */}
                                    {/* <Label>Reference Date</Label>
                            <Input /> */}
                                    <DatePicker
                                        defaultDate={new Date(2018, 4, 4)}
                                        minimumDate={new Date(2018, 1, 1)}
                                        maximumDate={new Date(2018, 12, 31)}
                                        locale={"en"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        placeHolderText="Reference Date"
                                        textStyle={{ color: "green" }}
                                        placeHolderTextStyle={{ color: "#d3d3d3" }}
                                        onDateChange={this.setDate}
                                        disabled={false}
                                    />
                                    {/* </Item> */}
                                </Body>
                            </CardItem>
                        </Card>
                        <Card style={styles.cardContainer}>
                            <CardItem header style={styles.cardContainer}>
                                <Text style={styles.cardHeaderText}>Attachments </Text>
                            </CardItem>
                            <CardItem style={styles.cardContainer}>
                                <Body>
                                    <View
                                        style={{
                                            borderBottomColor: 'black',
                                            borderBottomWidth: 3,
                                            marginTop: '-8%',
                                            alignSelf: 'stretch',
                                            width: '100%',
                                        }}
                                    />
                                </Body>
                            </CardItem>
                            <CardItem style={styles.cardContainer}>
                                <Body>
                                    <View style={styles.attachments}>
                                        <Button
                                            transparent
                                            style={styles.attachmentAlign}
                                            onPress={() => alert('pencil here')}>
                                            <FontAwesomeIcon icon={faPencilAlt} size="20" />
                                        </Button>
                                        <Button
                                            transparent
                                            style={styles.attachmentAlign}
                                            onPress={() => alert('camera here')}>
                                            <FontAwesomeIcon icon={faCamera} size="20" />
                                        </Button>
                                        <Button
                                            transparent
                                            style={styles.attachmentAlign}
                                            onPress={() => alert('mircophone here')}>
                                            <FontAwesomeIcon icon={faMicrophone} size="20" />
                                        </Button>
                                    </View>
                                </Body>

                            </CardItem>
                        </Card>
                    </ScrollView>
                </Content >
            </Container >

        );
    }
};

const styles = StyleSheet.create({
    subT: {
        fontSize: 10,
    },
    cardContainer: {
        flex: 1,
        width: '94%',
        alignSelf: 'center',
        borderRadius: 10,
    },
    Head: {
        fontSize: 26,
        fontWeight: 'bold',
    },
    cardHeaderText: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    input: {
        marginBottom: 20,
    },
    more: {
        alignSelf: 'flex-end',
    },
    moreText: {
        color: '#3399ff',
        fontSize: 16,
    },

    labelColor: {
        color: 'gray',
    },
    attachments: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignSelf: 'center',
        marginTop: '-14%',
        marginLeft: '10%',
    },
    attachmentAlign: {
        // marginTop: '-12%',
        // justifyContent: 'space-between',
        justifyContent: 'center',
        width: '16%',
        marginRight: '24%',
    },

});