import React, {useState} from 'react';
import {Container, Content, Form, Item, Input, Label} from 'native-base';
import {
  ImageBackground,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Axios from 'axios';
// import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
// import {faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons';

const resizeMode = 'center';
const NewPassword = ({route, navigation}) => {
  const [otp, setOtp] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  //   const handleEye = () => {
  //     if (userPassword !== '') {
  //       setShow(!show);
  //     }
  //   };
  const {mobile} = route.params;

  const resetHandle = () => {
    // console.log('confi:', confirmPassword, otp, newPassword);
    if (newPassword === confirmPassword) {
      Axios.get('http://139.59.3.114/act/api/logout').then(
        Axios.post(' http://139.59.3.114/act/api/resetpassword/', {
          device_id: '02:00:00:00:00:00',
          mobile: mobile,
          pwd: confirmPassword,
          otp: otp,
        }).then(res => {
          console.log('new', res);
          if (res.status === 200) {
            navigation.navigate('Dashboard');
          }
        }),
      );
    } else {
      alert("Your password's' does not match");
    }
  };

  return (
    <Container style={styles.container}>
      <ImageBackground
        style={{
          backgroundColor: '#ccc',
          flex: 1,
          resizeMode,
          position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        source={require('../assets/login_background.png')}>
        <Content>
          <Image
            style={styles.img}
            source={require('../assets/field_high.png')}
          />
          <Form style={styles.formContainer}>
            <Item floatingLabel last style={{width: 350}}>
              <Label style={styles.labelColor}>Enter Otp</Label>
              <Input
                style={styles.input}
                value={otp}
                onChangeText={value => setOtp(value)}
                keyboardType="number-pad"
              />
            </Item>
            <Item floatingLabel last style={{width: 350}}>
              <Label style={styles.labelColor}>New Password</Label>
              <Input
                style={styles.input}
                value={newPassword}
                onChangeText={value => setNewPassword(value)}
                secureTextEntry
              />
            </Item>
            <Item floatingLabel last style={{width: 350}}>
              <Label style={styles.labelColor}>Confirm Password</Label>
              <Input
                style={styles.input}
                value={confirmPassword}
                onChangeText={value => setConfirmPassword(value)}
                secureTextEntry
              />
            </Item>
            {/* <TouchableOpacity style={styles.eye} onPress={handleEye}>
              <FontAwesomeIcon
                icon={userPassword !== '' && show ? faEyeSlash : faEye}
              />
            </TouchableOpacity> */}
          </Form>
          <View style={styles.btnContainer}>
            <TouchableOpacity style={styles.userBtn} onPress={resetHandle}>
              <Text style={styles.btnTxt}>RESET PASSWORD</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </ImageBackground>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  img: {
    marginLeft: '10%',
    marginTop: '20%',
    height: 60,
    width: 240,
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '10%',
  },
  labelColor: {
    color: 'white',
  },
  input: {
    color: 'white',
  },
  btnContainer: {
    marginTop: '10%',
  },
  userBtn: {
    borderRadius: 8,
    backgroundColor: '#025678',
    padding: 16,
    width: '100%',
  },
  btnTxt: {
    textAlign: 'center',
    color: 'white',
  },
});

export default NewPassword;
