import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';

class Accordion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      updated_Height: 0,
      // backgroundColor: '#3399ff',
      //   click: false
    };
  }

  componentWillReceiveProps(update_Props) {
    if (update_Props.item.expanded) {
      this.setState(() => {
        return {
          updated_Height: null,
        };
      });
    } else {
      this.setState(() => {
        return {
          updated_Height: 0,
        };
      });
    }
  }

  shouldComponentUpdate(update_Props, nextState) {
    if (update_Props.item.expanded !== this.props.item.expanded) {
      return true;
    }
    return false;
  }

  handleNavigation = screenName => {
    if (screenName === 'Cash') {
      this.props.navigation.navigate('SearchList');
    } else if (screenName === 'Card') {
      this.props.navigation.navigate('ReceiptInCard');
    } else if (screenName === 'Cheque') {
      this.props.navigation.navigate('ReceiptCheque');
    }
  };

  render() {
    return (
      <View style={styles.accordionHolder}>
        <TouchableOpacity
          key={this.props.key}
          activeOpacity={0.7}
          onPress={this.props.onClickFunction}
          style={
            (styles.Button,
            {
              backgroundColor:
                this.props.color && this.props.item.expanded
                  ? '#0f49a6'
                  : this.props.item.bgColor,
              color:
                this.props.color && this.props.item.expanded
                  ? '#fff'
                  : this.props.item.color,
              padding: 10,
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            })
          }>
          <Text
            style={
              (styles.accordionButtonText,
              {
                backgroundColor:
                  this.props.color && this.props.item.expanded
                    ? '#0f49a6'
                    : this.props.item.bgColor,
                color:
                  this.props.color && this.props.item.expanded
                    ? '#fff'
                    : this.props.item.color,
              })
            }>
            {this.props.item.title}
          </Text>
          <Image
            source={
              this.props.color && this.props.item.expanded
                ? require('../assets/up-arrow.png')
                : require('../assets/down-arrow.png')
            }
            style={{
              width: 40,
              height: 40,
              backgroundColor:
                this.props.color && this.props.item.expanded
                  ? '#0f49a6'
                  : this.props.item.bgColor,
              color:
                this.props.color && this.props.item.expanded
                  ? 'white'
                  : this.props.item.color,
            }}
          />
        </TouchableOpacity>

        <View
          style={{
            flex: 1,
            height: this.state.updated_Height,
            overflow: 'hidden',
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor:
              this.props.color && this.props.item.expanded
                ? '#0f49a6'
                : this.props.item.bgColor,
            color:
              this.props.color && this.props.item.expanded
                ? '#fff'
                : this.props.item.color,
          }}>
          {this.props.item.buttonIcon.map(item => {
            return (
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => this.handleNavigation(item)}
                  style={{
                    width: '33.33%',
                    alignItems: "center",
                    // alignSelf: 'flex-end',
                    // justifyContent: 'space-between',
                    backgroundColor:
                      this.props.color && this.props.item.expanded
                        ? '#0f49a6'
                        : this.props.item.bgColor,
                    color:
                      this.props.color && this.props.item.expanded
                        ? 'white'
                        : this.props.item.color,
                  }}>
                  <Image
                    source={
                      item === 'Cash'
                        ? require('../assets/Cash.png')
                        : item === 'Card'
                          ? require('../assets/Card.png')
                          : require('../assets/Cheque.png')
                    }
                    style={{
                      width: 40,
                      height: 40,
                      backgroundColor:
                        this.props.color && this.props.item.expanded
                          ? '#0f49a6'
                          : this.props.item.bgColor,
                      color:
                        this.props.color && this.props.item.expanded
                          ? '#fff'
                          : this.props.item.color,
                    }}
                  />
                  <Text
                    style={
                      (styles.accordionText,
                      {
                        backgroundColor:
                          this.props.color && this.props.item.expanded
                            ? '#0f49a6'
                            : this.props.item.bgColor,
                        color:
                          this.props.color && this.props.item.expanded
                            ? '#fff'
                            : this.props.item.color,
                      })
                    }>
                    {item}{' '}
                  </Text>
                </TouchableOpacity>
            );
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  accordionText: {
    fontSize: 18,
    // color: '#ffffff',
    padding: 10,
    // backgroundColor: "#ffffff"
  },
  accordionButtonText: {
    textAlign: 'left',
    // color: "#fff",
    fontSize: 21,
    // backgroundColor: "#ffffff"
  },
  accordionHolder: {
    borderWidth: 1,
    // borderColor: '#0370fc',
    // backgroundColor: "#ffffff",
    marginVertical: 5,
  },
  Button: {
    padding: 10,
    // backgroundColor: '#0370fc',
  },
});

export default Accordion;
