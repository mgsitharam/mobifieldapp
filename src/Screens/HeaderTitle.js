import React, { Component } from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Subtitle,
  Button,
  Icon,
} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
// import {faBell, faEllipsisV, faBackward, faArrowLeft} from '@fortawesome/free-solid-svg-icons';

const HeaderTitle = props => {
  console.log(props);
  return (
    <Header style={{ backgroundColor: '#3399ff' }}>
      <Left>
        <Button transparent>
          {/* <FontAwesomeIcon icon={props.icon} style={{color: 'white'}} /> */}
        </Button>
      </Left>
      <Body>
        <Title>{props.title}</Title>
        <Subtitle>{props.subTitle}</Subtitle>
      </Body>
      <Right>
        <Button
          transparent
          onPress={() => props.navigation.navigate('Notification')}>
          <FontAwesomeIcon
            icon={props.notification}
            size="20"
            style={{
              color: 'white',
            }}
          />
        </Button>
        <Button transparent onPress={props.menuPress}>
          <FontAwesomeIcon
            icon={props.menu}
            size="20"
            style={{ color: 'white' }}
          />
        </Button>
      </Right>
    </Header>
  );
};

export default HeaderTitle;
