import React, { Component } from 'react';
import { View, ScrollView, Text } from 'react-native';
import { Container, Content, Button, Left, Right, Body, Header } from 'native-base';
import { TextInput } from 'react-native-gesture-handler';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faPaperPlane, faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';

export default class SearchList extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        console.log(this.props);
        return (
            <Container>
                {/* <HeaderTitle notification={faPaperPlane} {...props} /> */}
                <Header style={{ backgroundColor: '#3399ff' }}>
                    <Left>
                        <Button
                        transparent
                        onPress={() => {  console.log("back clicks"); this.props.navigation.goBack()}}>
                            <FontAwesomeIcon
                                icon={faArrowLeft}
                                size="20"
                                style={{
                                    color: 'white',
                                }}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <TextInput />
                    </Body>
                    <Right>
                        <Button
                            transparent
                            onPress={() => { console.log("search clicks"); }}>
                            <FontAwesomeIcon
                                icon={faSearch}
                                size="20"
                                style={{
                                    color: 'white',
                                }}
                            />
                        </Button>
                        <Button
                            transparent
                            onPress={() => { console.log("Add clicks"); this.props.navigation.navigate("CashInput")}}>
                            <FontAwesomeIcon
                                icon={faPlus}
                                size="20"
                                style={{
                                    color: 'white',
                                }}
                            />
                        </Button>
                    </Right>
                </Header>


                <Content>

                    <ScrollView style={{ backgroundColor: 'red' }}>
                        <View style={{ backgroundColor: 'blue' }} >
                            <Text> hi there</Text>
                        </View>
                        <View style={{ backgroundColor: 'green' }}>
                            <Text> hi there</Text>
                        </View>
                        <View style={{ backgroundColor: 'pink' }} >
                            <Text> hi there</Text>
                        </View>
                    </ScrollView>
                </Content>
            </Container>

        );
    }
}