import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Modal from 'react-native-modal';
import {Container, Content, ListItem, Separator, List} from 'native-base';

class SwitchLocation extends Component {
  state = {
    isModalVisible: false,
    list: [
      {
        location: 'AL Vijayawada',
      },
      {
        location: 'Guntur',
      },
      {
        location: 'Tenali',
      },
    ],
  };

  openModal = () => {
    this.setState({
      isModalVisible: true,
    });
  };

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    });
  };

  closeModal = () => {
    this.setState({
      isModalVisible: false,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{marginTop: 50}}>
          <TouchableOpacity onPress={() => this.openModal()}>
            <Text style={styles.txt}>Click to Switch Location</Text>
          </TouchableOpacity>
          <Modal
            isVisible={this.state.isModalVisible}
            style={{
                marginTop: 200,
              backgroundColor: 'white',
              maxHeight: Dimensions.get('window').height / 2,
              justifyContent: 'center',
              //   alignItems: 'center',
            }}
            onBackdropPress={() => this.closeModal()}
            // swipeDirection="right"
            animationIn="slideInLeft"
            animationOut="slideOutDown">
            <Container>
              <Content>
                <Separator bordered>
                  <Text>Select A Location</Text>
                </Separator>
                {this.state.list.map(l => (
                  <ListItem activeOpacity={0.3} key={l.name} style={{alignItems: "center"}}>
                    <Text>{l.location}</Text>
                  </ListItem>
                ))}
              </Content>
            </Container>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ccc',
  },
  txt: {
    color: 'white',
    fontSize: 30,
  },
});

export default SwitchLocation;
