import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  UIManager,
  ScrollView,
  LayoutAnimation,
  Platform,
  TouchableOpacity,
} from 'react-native';
import { Container, Content, Image, Button } from 'native-base';
import HeaderTitle from './HeaderTitle';
// import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faBell, faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import Accordion from './Accordion.js';
import CompanyList from '../../companySwitch';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    const array = [
      {
        expanded: false,
        title: 'RECEIPT',
        buttonIcon: ['Cash', 'Card', 'Cheque'],
        bgColor: 'white',
        color: 'black',
      },
      {
        expanded: false,
        title: 'PAYMENT',
        buttonIcon: ['Cash', 'Cheque'],
        bgColor: 'white',
        color: 'black',
      },
      {
        expanded: false,
        title: 'TRANSFER',
        buttonIcon: ['Cash', 'Cheque'],
        bgColor: 'white',
        color: 'black',
      },
      {
        expanded: false,
        title: 'DEPOSIT',
        buttonIcon: ['Cash', 'Cheque'],
        bgColor: 'white',
        color: 'black',
      },
      {
        expanded: false,
        title: 'REGISTER',
        buttonIcon: ['Cash', 'Card', 'Cheque'],
        bgColor: 'white',
        color: 'black',
      },
    ];

    this.state = {
      AccordionData: [...array],
      showList: false,
      // color: false
    };
  }

  showCompany(e) {
    console.log(this.state.showList);
    this.setState({ showList: !this.state.showList });
  }

  showNotification(e) {
    console.log('show notification');
  }

  updateLayout = index => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const array = this.state.AccordionData.map(item => {
      const newItem = Object.assign({}, item);
      newItem.expanded = false;
      return newItem;
    });

    array[index].expanded = true;

    this.setState(() => {
      return { AccordionData: array, color: true };
    });
  };

  render() {
    return (
      <Container>
        <HeaderTitle
          notification={faBell}
          menu={faEllipsisV}
          title="AL Vijayawada"
          subTitle="Toli Chowki"
          menuPress={e => this.showCompany(e)}
          {...this.props}
        />
        <Content padder>
          <ScrollView>
            <CompanyList show={this.state.showList} {...this.props} />
            {this.state.AccordionData.map((item, key) => (
              <Accordion
                key={key}
                color={this.state.color}
                onClickFunction={this.updateLayout.bind(this, key)}
                item={item}
                {...this.props}
              />
            ))}
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  text: {},
  content: {
    // marginTop: -760,
  },
});

export default Dashboard;
