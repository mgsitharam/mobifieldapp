import React, {useState} from 'react';

import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Title,
  Subtitle,
  Button,
  Card,
  CardItem,
  Input,
  Item,
  Label,
} from 'native-base';
import {ScrollView} from 'react-native-gesture-handler';
import {
  faPaperPlane,
  faArrowLeft,
  faPencilAlt,
  faCamera,
  faMicrophone,
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

const ReceiptInCard = (props, {navigation}) => {
  const [validItems, setvalidItems] = useState(false);

  return (
    <Container>
      {/* <HeaderTitle notification={faPaperPlane} {...props} /> */}
      <Header style={{backgroundColor: '#3399ff'}}>
        <Left>
          <FontAwesomeIcon
            icon={faArrowLeft}
            size="20"
            style={{
              color: 'white',
            }}
          />
        </Left>
        <Body>
          <Title>Card Receipt</Title>
          <Subtitle style={styles.subT}>AL Vijayawada - Mobibooks</Subtitle>
        </Body>
        <Right>
          <Button
            transparent
            onPress={() => props.navigation.navigate('Notification')}>
            <FontAwesomeIcon
              icon={faPaperPlane}
              size="20"
              style={{
                color: 'white',
              }}
            />
          </Button>
        </Right>
      </Header>
      <Content style={{backgroundColor: 'gainsboro'}}>
        <ScrollView>
          <Card style={styles.cardContainer}>
            <CardItem header style={styles.cardContainer} transparent>
              <Text style={styles.cardHeaderText}>DayBook**</Text>
            </CardItem>
            <CardItem style={styles.cardContainer} transparent>
              <Body>
                <Item floatingLabel style={{width: 350}}>
                  <Label style={styles.labelColor}>Password</Label>
                  <Input
                    style={styles.input}
                    // value={userPassword}
                    onChangeText={() => console.log('hi')}
                  />
                </Item>
              </Body>
            </CardItem>
          </Card>
          <Card style={styles.cardContainer}>
            <CardItem header style={styles.cardContainer} transparent>
              <Text style={styles.cardHeaderText}>Received From *</Text>
            </CardItem>
            <CardItem style={styles.cardContainer} transparent>
              <Body>
                <View style={{marginTop: '-10%'}} />
                <Item floatingLabel style={{width: 350}}>
                  <Label style={styles.labelColor}>Party Name*</Label>
                  <Input
                    style={styles.input}
                    // value={userPassword}
                    onChangeText={() => console.log('hi')}
                  />
                </Item>
                <Item floatingLabel style={{width: 350}}>
                  <Label style={styles.labelColor}>Mobile*</Label>
                  <Input
                    style={styles.input}
                    // value={userPassword}
                    onChangeText={() => console.log('hi')}
                  />
                </Item>
              </Body>
            </CardItem>
            {validItems ? (
              <CardItem style={styles.cardContainer} transparent>
                <Body>
                  <View style={{marginTop: '-6%'}}></View>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>Print Name</Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>Reference Name</Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>Email</Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>
                      Billing Address Line 1
                    </Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>
                      Billing Address Line 2
                    </Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>City</Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>City</Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>Pin</Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>STATE</Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>PAN</Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                  <Item floatingLabel style={{width: 350}}>
                    <Label style={styles.labelColor}>GSTIN</Label>
                    <Input
                      style={styles.input}
                      // value={userPassword}
                      onChangeText={() => console.log('hi')}
                    />
                  </Item>
                </Body>
              </CardItem>
            ) : (
              <TouchableOpacity
                style={styles.more}
                onPress={() => setvalidItems(!validItems)}>
                <Text style={styles.moreText}>MORE</Text>
              </TouchableOpacity>
            )}

            <CardItem header>
              <Text style={styles.cardHeaderText}>Customer Type</Text>
            </CardItem>
            <CardItem style={styles.cardContainer}>
              <Body style={{marginTop: '-10%'}}>
                <Item floatingLabel style={{width: 350}}>
                  <Label style={styles.labelColor}>Unregistered</Label>
                  <Input
                    style={styles.input}
                    // value={userPassword}
                    onChangeText={() => console.log('hi')}
                  />
                </Item>
              </Body>
            </CardItem>
          </Card>
          <Card style={styles.cardContainer}>
            <CardItem style={styles.cardContainer} transparent>
              <Body>
                <Item floatingLabel style={{width: 350}}>
                  <Label style={styles.labelColor}>Amount*</Label>
                  <Input
                    style={styles.input}
                    // value={userPassword}
                    onChangeText={() => console.log('hi')}
                  />
                </Item>
                <Item floatingLabel style={{width: 350}}>
                  <Label style={styles.labelColor}>Narration</Label>
                  <Input
                    style={styles.input}
                    // value={userPassword}
                    onChangeText={() => console.log('hi')}
                  />
                </Item>
                <Item floatingLabel style={{width: 350}}>
                  <Label style={styles.labelColor}>Reference Number</Label>
                  <Input
                    style={styles.input}
                    // value={userPassword}
                    onChangeText={() => console.log('hi')}
                  />
                </Item>
                <Item floatingLabel style={{width: 350}}>
                  <Label style={styles.labelColor}>Reference Date</Label>
                  <Input
                    style={styles.input}
                    // value={userPassword}
                    onChangeText={() => console.log('hi')}
                  />
                </Item>
              </Body>
            </CardItem>
          </Card>
          <Card style={styles.cardContainer}>
            <CardItem header style={styles.cardContainer}>
              <Text style={styles.cardHeaderText}>Attachments </Text>
            </CardItem>
            <CardItem style={styles.cardContainer}>
              <Body>
                <View
                  style={{
                    borderBottomColor: 'black',
                    borderBottomWidth: 3,
                    marginTop: '-8%',
                    alignSelf: 'stretch',
                    width: '100%',
                  }}
                />
              </Body>
            </CardItem>
            <CardItem style={styles.cardContainer}>
              <Body>
                <View style={styles.attachments}>
                  <Button
                    transparent
                    style={styles.attachmentAlign}
                    onPress={() => alert('pencil here')}>
                    <FontAwesomeIcon icon={faPencilAlt} size="20" />
                  </Button>
                  <Button
                    transparent
                    style={styles.attachmentAlign}
                    onPress={() => alert('camera here')}>
                    <FontAwesomeIcon icon={faCamera} size="20" />
                  </Button>
                  <Button
                    transparent
                    style={styles.attachmentAlign}
                    onPress={() => alert('mircophone here')}>
                    <FontAwesomeIcon icon={faMicrophone} size="20" />
                  </Button>
                </View>
              </Body>
            </CardItem>
          </Card>
          {/* <View style={{marginTop: '10%'}}></View> */}
        </ScrollView>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  subT: {
    fontSize: 10,
  },
  cardContainer: {
    flex: 1,
    width: '94%',
    alignSelf: 'center',
    borderRadius: 10,
  },
  cardHeaderText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  input: {
    marginBottom: 20,
  },
  more: {
    alignSelf: 'flex-end',
  },
  moreText: {
    color: '#3399ff',
    fontSize: 16,
  },

  labelColor: {
    color: 'gray',
  },
  attachments: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // alignSelf: 'center',
    marginTop: '-14%',
    marginLeft: '10%',
  },
  attachmentAlign: {
    // marginTop: '-12%',
    // justifyContent: 'space-between',
    justifyContent: 'center',
    width: '16%',
    marginRight: '24%',
  },
});

export default ReceiptInCard;
