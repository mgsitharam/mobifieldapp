/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import {Container, Content, Form, Item, Input, Label, Icon} from 'native-base';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons';
import Axios from 'axios';
import ResetPassword from './ResetPassword';

const Login = ({navigation}, props) => {
  const [userMobile, setUserMobile] = useState('9108152231');
  const [userPassword, setUserPassword] = useState('');
  const [show, setShow] = useState(true);
  // cons [status]

  useEffect(() => {
    console.log('mount');
    return () => setUserPassword('');
  }, []);

  useEffect(() => {
    // effect
    console.log('update');

    // return () => {
    //   setUserPassword('');
    // };
  }, [userPassword]);

  const loginHandle = e => {
    Axios.post('http://139.59.3.114/act/api/login/', {
      mobile: userMobile,
      password: userPassword,
      device_id: '02:00:00:00:00:00',
    })
      .then(res => {
        // console.log(JSON.parse(res.config.data.mobile));
        if (res.status === 200) {
          navigation.navigate('Dashboard', {refresh: unMount});
        }
      })
      .catch(err => {
        throw err;
      });
  };

  const unMount = () => {
    setUserPassword('');
  };

  const handleEye = () => {
    if (userPassword !== '') {
      setShow(!show);
    }
  };

  const handleReset = () => {
    navigation.navigate('ResetPassword', {mobile: userMobile});
  };

  const resizeMode = 'center';
  console.log('userPassword::', userPassword);
  return (
    <Container style={styles.container}>
      {/* <StatusBar backgroundColor="#1e90ff" barStyle="light-content" /> */}
      <ImageBackground
        style={{
          backgroundColor: '#ccc',
          flex: 1,
          resizeMode,
          position: 'absolute',
          width: '100%',
          height: '100%',
          alignItems: 'center',
        }}
        source={require('../assets/login_background.png')}>
        <Content>
          <Image
            style={styles.img}
            source={require('../assets/field_high.png')}
          />
          <Form style={styles.formContainer}>
            <Item floatingLabel last style={{width: 350}}>
              <Label style={styles.labelColor}>User Mobile</Label>
              <Input
                style={styles.input}
                value={userMobile}
                onChangeText={value => setUserMobile(value)}
                keyboardType="number-pad"
              />
            </Item>
            <Item floatingLabel last style={{width: 350}}>
              <Label style={styles.labelColor}>Password</Label>
              <Input
                style={styles.input}
                value={userPassword}
                onChangeText={value => setUserPassword(value)}
                // keyboardType="ascii-capable"
                secureTextEntry={show}
              />
            </Item>
            <TouchableOpacity style={styles.eye} onPress={handleEye}>
              <FontAwesomeIcon
                icon={userPassword !== '' && show ? faEyeSlash : faEye}
                size="20"
              />
            </TouchableOpacity>
          </Form>
          <View style={styles.btnContainer}>
            <TouchableOpacity style={styles.userBtn} onPress={loginHandle}>
              <Text style={styles.btnTxt}>LOGIN</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={handleReset}>
            <Text style={styles.btnEnd}>Reset Password?</Text>
          </TouchableOpacity>
          <Text style={styles.btnEnd}>Version 2.7.1</Text>
        </Content>
      </ImageBackground>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  img: {
    marginLeft: '10%',
    marginTop: '20%',
    height: 60,
    width: 240,
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '10%',
  },
  labelColor: {
    color: 'white',
  },
  input: {
    color: 'white',
  },
  btnContainer: {
    marginTop: '8%',
  },
  userBtn: {
    borderRadius: 8,
    backgroundColor: '#025678',
    padding: 16,
    width: '100%',
  },
  btnTxt: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
  },
  btnEnd: {
    color: 'white',
    marginTop: 30,
    textAlign: 'center',
  },
  eye: {
    marginTop: '-8%',
    alignSelf: 'flex-end',
  },
});

export default Login;
