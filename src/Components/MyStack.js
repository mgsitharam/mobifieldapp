/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../Screens/Login';
import Dashboard from '../Screens/Dashboard';

import Notification from '../Screens/Notification';
import ResetPassword from '../Screens/ResetPassword';
import HeaderTitle from '../Screens/HeaderTitle';
import NewPassword from '../Screens/NewPassword';
import SplashScreen from '../Screens/SplashScreen';
import ReceiptInCard from '../Screens/ReceiptInCard';
import CashInput from '../Screens/cashInput';
import SearchList from '../Screens/searchList';
import ReceiptCheque from '../Screens/receiptCheque'
import { TextInput } from 'react-native-gesture-handler';


const Stack = createStackNavigator();

const MyStack = React$Node => {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SplashScreen">
          <Stack.Screen
            name="SplashScreen"
            component={SplashScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="ResetPassword"
            component={ResetPassword}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Dashboard"
            component={Dashboard}
            options={({navigation, route}) => ({
              headerTitle: props => <HeaderTitle {...props} />,
              headerShown: false,
              title: 'AL Vijayawada',
              headerLeft: null,
              headerStyle: {
                backgroundColor: '#3399ff',
              },
              headerTitleStyle: {
                color: 'white',
              },
            })}
          />
          <Stack.Screen
            name="Notification"
            component={Notification}
            options={{
              title: 'Committed Transactions',
              headerStyle: {
                backgroundColor: '#3399ff',
              },
              headerTitleStyle: {
                color: 'white',
              },
              headerTintColor: '#fff',
            }}
          />
          <Stack.Screen
            name="NewPassword"
            component={NewPassword}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="ReceiptInCard"
            component={ReceiptInCard}
            options={{headerShown: false, headerLeft: null}}
          />
          <Stack.Screen
            name="CashInput"
            component={CashInput}
            options={{
              title: 'Cash Receipt',
              headerShown: true,
              headerStyle: {
                backgroundColor: '#3399ff',
              },
              headerTitleStyle: {
                color: 'white',
              },
              headerTintColor: '#fff',
            }}
            />
            <Stack.Screen
            name="ReceiptCheque"
            component={ReceiptCheque}
            options={{
              title: 'Cheque Receipt',
              headerShown: true,
              headerStyle: {
                backgroundColor: '#3399ff',
              },
              headerTitleStyle: {
                color: 'white',
              },
              headerTintColor: '#fff',
            }}
            />
            <Stack.Screen
            name="SearchList"

            component={SearchList }
            options={{
              headerShown: false,
              headerStyle: {
                backgroundColor: '#3399ff',
              },
              headerTintColor: '#fff'
            }}
            />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default MyStack;
