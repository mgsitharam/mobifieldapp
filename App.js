import 'react-native-gesture-handler';
import React, {Component} from 'react';
import MyStack from './src/Components/MyStack';
import Axios from 'axios';

export default class App extends Component {
  componentDidMount() {
    Axios.get('http://139.59.3.114/act/api/logout').then(response =>
      console.log('logot', response),
    );
  }
  render() {
    return (
      <>
        <MyStack />
      </>
    );
  }
}
