import React, { Component } from "react";
import {
  Content,
  Card,
  CardItem,
  Container,
  Separator,
  List,
  ListItem
} from "native-base";
import { View, Text, TouchableOpacity, Dimensions } from "react-native";
import Modal from "react-native-modal";
import Loader from './src/Screens/loader'

export default class CompanyList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaderShow: false,
      show: false,
      isModalVisible: false,
      isCompanyModalVisible: false,
      resp: {},
      resp2: {},
      tenantResp: []
    };
  }

  callLogout() {
    this.setState({ loaderShow: true })
    fetch('http://139.59.3.114/act/api/logout/', {
      method: 'GET',
    }).then((response) => response.json())
      .then(response => {
        // this.setState({ loaderShow: false })
        console.log("logout response: ", response)
      })
      .catch(error => {
        // this.setState({ loaderShow: false })
        console.log("error logout: ", error)
      });
    console.log("logged out ---------")
  }
  callLogin() {
    this.setState({ loaderShow: true })
    let data = new FormData();
    data.append('device_id', '02:00:00:00:00:00')
    data.append('password', 'Mobi1')
    data.append('mobile', '9108152231')
    console.log("login data: ", data)
    return fetch('http://139.59.3.114/act/api/login/', {
      method: 'POST',

      body: data
    }).then((response) => response.json())
      .then((response) => {
        console.log(data)
        console.log(typeof response);
        console.log("*****************************************************************************************")
        console.log(response);
        if ("error" in response) {
          console.log("Error in login: ", response.error)
        } else {
          this.setState({
            resp: response
          })
          console.log("completes login: ", response)

          this.openModal("company", "companycc");
          // this.setState({ loaderShow: false })
          return response
        }
      })
      .catch((error) => {
        console.log("Error: ", error)
        // this.setState({ loaderShow: false })
      });
  }

  getTenantList() {
    this.setState({ loaderShow: true })
    fetch('http://139.59.3.114/act/core/tenant/', {
      method: 'GET',
      headers: {
        'X-CSRFToken': this.state.resp.csrf,
        'session_id': this.state.resp.session_key,
        'Content-Type': 'application/json',
      },
    }).then(response => response.json())
      .then(responseJson => {
        console.log("tenant response: ", responseJson)
        if ("error" in responseJson) {
          console.log("error in response: ", responseJson.error)
          this.setState({
            tenantResp: {}
          })
        } else {
          this.setState({
            tenantResp: responseJson
          })
          this.openModal("company", "companyll")
        }
        // this.setState({ loaderShow: false })
      })
      .catch(error => {
        // this.setState({ loaderShow: false })
        console.log("error in getTenant: ", error)
      });
  }
  async getCompany() {
    // const self =this;
    console.log("get company");
    this.setState({loaderShow: true})

    await this.callLogout();

    let res = await this.callLogin();
    console.log('res:', res);
    if (res){
      await this.getTenantList();
      this.setState({loaderShow: false})
    }
  }

  getLoacation(tenant) {
    this.setState({ loaderShow: true })
    console.log("get location")
    console.log("tenant: ", tenant)

    fetch('http://139.59.3.114/act/api/second_lg/', {
      method: 'POST',
      headers: {
        "X-CSRFToken": this.state.resp.csrf,
        "session_id": this.state.resp.session_key,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        "device_id": "02:00:00:00:00:00",
        "tenant_name": tenant.name,
        "tenant_id": tenant.id
      }),
    }).then(response => response.json())
      .then(responseJson => {
        console.log(responseJson);
        if ("error" in responseJson) {
          this.setState({
            resp2: {},
          });
          this.setState({ loaderShow: false })
        } else {
          this.setState({
            resp2: responseJson,
          });
          this.setState({ loaderShow: false })
          this.openModal("location", "")
        }
      })
      .catch(error => {
        console.log("catches error in location:", error);
        this.setState({ loaderShow: false })
      });
  }

  openModal = (item, company) => {
    console.log(company)
    if (item === "location") {
      this.setState({
        isModalVisible: true,
        isCompanyModalVisible: false
      });
    } else {
      console.log("Call company **")
      this.setState({
        isModalVisible: false,
        isCompanyModalVisible: true
      });
    }

  };

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      isCompanyModalVisible: !this.state.isCompanyModalVisible,
    });
  };

  closeModal = () => {
    this.setState({
      isModalVisible: false,
      isCompanyModalVisible: false,
      show: false
    });
  };

  selectCompany = e => {
    console.log("select company");
  };
  render() {
    return (
      <Content>
        {this.props.show ? (
          <View>
            <Card>
              <TouchableOpacity onPress={() => this.openModal("location", "companyll")}>
                <CardItem>
                  <Text>Switch Location</Text>
                </CardItem>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.getCompany()}>
                <CardItem>
                  <Text>Switch Company</Text>
                </CardItem>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                <CardItem>
                  <Text>Logout</Text>
                </CardItem>
              </TouchableOpacity>
            </Card>
            <Modal
              isVisible={this.state.isModalVisible}
              style={{
                marginTop: 200,
                backgroundColor: "white",
                maxHeight: Dimensions.get("window").height / 2,
                justifyContent: "center"
              }}
              onBackdropPress={() => this.closeModal()}
              animationIn="slideInLeft"
              animationOut="slideOutDown"
            >
              <Container>
                <Content>
                  <Separator bordered>
                    <Text>Select A Location</Text>
                  </Separator>
                  <List>
                    {this.state.resp2 !== '' && this.state.resp2.location && this.state.resp2.location.map(location => (
                      <ListItem
                        activeOpacity={0.3}
                        key={location.name}
                        style={{ alignItems: "center" }}
                      >
                        <Text>{location.display_name}</Text>
                      </ListItem>
                    ))}
                  </List>
                </Content>
              </Container>
            </Modal>

            <Modal
              isVisible={this.state.isCompanyModalVisible}
              style={{
                marginTop: 200,
                backgroundColor: "white",
                maxHeight: Dimensions.get("window").height / 2,
                justifyContent: "center"
              }}
              onBackdropPress={() => this.closeModal()}
              animationIn="slideInLeft"
              animationOut="slideOutDown"
            >
              <Container>
                <Content>
                  <Separator bordered>
                    <Text>Select A Company</Text>
                  </Separator>
                  <List>
                    {this.state.tenantResp && (this.state.tenantResp.length > 0) && this.state.tenantResp.map(tenant => {
                      return <ListItem
                        activeOpacity={0.3}
                        key={tenant.name}
                        style={{ alignItems: "center" }}
                        onPress={() => this.getLoacation(tenant)}
                      >
                        <Text>{tenant.tenant_name}</Text>
                      </ListItem>
                    })}
                  </List>
                </Content>
              </Container>
            </Modal>
          </View>
        ) : null}
        <Modal
          isVisible={this.state.loaderShow}
          style={{
            // marginTop: 200,
            backgroundColor: "teransparent",
            maxHeight: Dimensions.get("window").height,
            justifyContent: "center",
            // zIndex: -1
          }}
          // onBackdropPress={() => this.closeModal()}
          animationIn="fadeIn"
          animationOut="fadeOut"
        >
          <Loader />
          {/* {this.state.loaderShow && <Loader />} */}
        </Modal>
      </Content>
    );
  }
}
